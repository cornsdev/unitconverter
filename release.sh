#!/bin/bash

mykey='../keys/cornsdev.jks'
unsigned_apk=./platforms/android/build/outputs/apk/android-release-unsigned.apk

export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)
cordova build --release android


jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $mykey $unsigned_apk cornsdev
~/Library/Android/sdk/build-tools/24.0.1/zipalign -v 4 $unsigned_apk ./app-release.apk
