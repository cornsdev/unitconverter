// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ngSanitize'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    if( ionic.Platform.isAndroid() )  {
      admobid = { banner: 'ca-app-pub-3050996155258099/8311250165'};
      if(AdMob) {
        AdMob.createBanner( {
          adId:admobid.banner,
          position:AdMob.AD_POSITION.BOTTOM_CENTER,
          autoShow:true
        });
      }
    }

  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/history.html',
    controller: 'AppCtrl'
  })


    .state('app.converters', {
      url: '/converters',
      views: {
        'menuContent': {
          templateUrl: 'templates/converters.html',
          controller: 'ConvertersCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/converters/:converter?d=:displays',
    views: {
      'menuContent': {
        templateUrl: 'templates/converter.html',
        controller: 'ConverterCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/converters');
});
