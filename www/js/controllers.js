angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  $scope.converters = [];
  for (key in CONVERTERS) {
    $scope.converters.push(key);
  }
})

.controller('ConvertersCtrl', function($scope) {
  $scope.converters = [];
  for (key in CONVERTERS) {
    $scope.converters.push(key);
  }
})

.controller('ConverterCtrl', function($scope, $stateParams) {
  $scope.converter = $stateParams.converter;

  displays = $stateParams.d
  displays = displays?displays.split(','):['0:1','1']

  $scope.displays = []
  for (u in CONVERTERS[$scope.converter]) {
    display = CONVERTERS[$scope.converter][u]
    display.idx = u
    display.symbol = toFomula(display.symbol[0])
    display.value = 1/display.ratio
    $scope.displays.push(display);
  }

  $scope.onValueChange = function(i) {
    displays = $scope.displays
    b = i.value * i.ratio
    for (d in displays) {
      if (d == i.idx) continue;
      $scope.displays[d].value = (b / displays[d].ratio)
    }
  }

  //init
  base = $scope.displays[0]
  base.value = 1
  $scope.onValueChange(base)
});

function toFomula(s) {
  return s.replace(/\^\-?\d+/g, function sup(x){
    return "<sup>"+x.substring(1)+"</sup>";
  });
}
