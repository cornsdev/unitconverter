import os, io
from lxml import html
import requests
import json


#
def extract(url):
    page = requests.get(url)
    tree = html.fromstring(page.content)

    d = dict( (a.text[:-10].strip(), a.attrib['href']) for a in tree.xpath('//div[@id="content"]/ul/li/a') )
    del d['Numbers']
    return d


def extractCategory(category, url):

    page = requests.get(url)
    tree = html.fromstring(page.content)

    li = tree.xpath('//ul[@class="allunits"]/li/text()')
    b = grepData("1 " + li[0])
    base = {'name': b[1], 'ratio': 1}
    if (b[2]):
        base['symbol'] = b[2]

    units = [base]
    for i in li[1:]:
        u = i.split('=')

        l = grepData(u[0])
        r = grepData(u[1])

        res = {'name': l[1], 'ratio': r[0]/l[0] }
        if (l[2]):
            res['symbol'] = l[2]
            units.append(res)


    return units



def grepData(s):
    d = s.strip().split(' ', 1)
    num = float(d[0])

    b1 = d[1].find('[')
    if b1 == -1:
        return num, d[1].strip(), None
    else:
        return num, d[1][:b1].strip(), d[1][b1+1:-1].split(', ')

def main():
    res = {}
    c = extract('http://www.unitconverters.net/common-converters.html')
    for key, value in c.iteritems():
        url = "http://www.unitconverters.net%s"%value
        res[key] = extractCategory(key, url)

    print json.dumps(res)


main()
